﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Hello World");
    }

    // Update is called once per frame
    void Update()
    {
        print(Time.deltaTime);
        if(Time.deltaTime > 20)
        {
            for (int i = 0; i < 22; i++)
            {
                Debug.Log("Hello there!");
                print("General KENOBI!?");
            }
        }
    }
}
